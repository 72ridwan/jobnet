from django.conf.urls import url
from .views import index, process_profile

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^process_profile', process_profile, name='process_profile'),
]
