from django.shortcuts import render
from login.views import response as respon 
from django.views.decorators.csrf import csrf_exempt
response = {}

def index(request):
	response['author'] = "Kelompok 15 PPW"
	return render(request, "t3_profile/t3_profile.html", response)
    
@csrf_exempt
def process_profile(request):
    response['type'] = request.POST.get('type')
    response['name'] = request.POST.get('name')
    response['id'] = request.POST.get('id')
    response['descriptions'] = request.POST.get('description', '')
    response['weburl'] = request.POST.get('weburl', '')
    response['imgurl'] = request.POST.get('logourl', '')
    response['specialties'] = request.POST.get('specialties', '')
    return render(request, 't3_profile/t3_profile_for_ajax.html', response)
