from django.apps import AppConfig


class T3ProfileConfig(AppConfig):
    name = 't3_profile'
