from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, process_profile
# Create your tests here.
class ProfileUnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)
    
    def test_coverage(self):
        Client().post('/profile/process_profile', {'type':'test type', 'name':'test name', 'id':'test id'})